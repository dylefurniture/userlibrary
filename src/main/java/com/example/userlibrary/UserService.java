package com.example.userlibrary;

import java.util.List;
import java.util.UUID;

public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    List<User> insertRandomUser(UUID userId) {
        userRepository.save(User.builder().id(userId).build());

        return userRepository.findAll();
    }
}
