package com.example.userlibrary;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories
public class Conf {

    @Bean
    UserService userService(UserRepository userRepository){
        return new UserService(userRepository);
    }
}
