package com.example.userlibrary;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ContextConfiguration(classes = Conf.class)
class UserServiceTest {

    @Autowired UserService userService;

    @Test
    void shouldInsertRandomUserAndReturnAllUsers() {
        final UUID uuid = UUID.randomUUID();
        final User result = userService.insertRandomUser(uuid)
                .stream()
                .findFirst()
                .orElseThrow(() -> new AssertionError("user has to be inserted"));

        assertThat(result.getId()).isEqualTo(uuid);
    }
}